//
//  ProductsViewControllerTests.swift
//  tabBarIbkTests
//
//  Created by Derly Edwin Ticse Zuñiga on 15/06/24.
//

import XCTest
@testable import tabBarIbk
import UIKit
import JGProgressHUD
import Foundation
class ProductsViewControllerTests: XCTestCase {
    
    var viewController: ProductsViewController!
    var mockPresenter: MockProductsPresenter!
    
    override func setUp() {
        super.setUp()
        viewController = ProductsViewController()
        mockPresenter = MockProductsPresenter()
        viewController.setPresenter(presenter: mockPresenter)
        viewController.loadViewIfNeeded()  // Carga la vista para inicializar las IBOutlet y otras configuraciones
    }
    
    override func tearDown() {
        viewController = nil
        mockPresenter = nil
        super.tearDown()
    }
    
    func testViewDidLoad() {
        viewController.viewDidLoad()
        
        // Verifica que el método initTableView() haya sido llamado
        // Esto se puede verificar indirectamente al asegurarse de que las propiedades de la tabla estén configuradas
        XCTAssertNotNil(viewController.productTB.delegate)
        XCTAssertNotNil(viewController.productTB.dataSource)
        XCTAssertEqual(viewController.productTB.rowHeight, 80)
    }
    
    func testStartLoading() {
        viewController.startLoading()
        XCTAssertTrue(viewController.loader.isVisible)
    }
    
    func testFinishedLoading() {
        viewController.finishedLoading()
        XCTAssertFalse(viewController.loader.isVisible)
    }
    
    func testCargarProducto() {
        viewController.cargarProducto(offset: "0", limit: "10")
        XCTAssertTrue(mockPresenter.getProductCalled)
    }
    
}
