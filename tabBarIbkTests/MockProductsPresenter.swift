//
//  MockProductsPresenter.swift
//  tabBarIbkTests
//
//  Created by Derly Edwin Ticse Zuñiga on 15/06/24.
//

import Foundation
import XCTest
@testable import tabBarIbk
import UIKit
import JGProgressHUD

// Mock del presenter
class MockProductsPresenter: ProductsPresenterProtocol {
    var getProductCalled = false
    
    func getproducto(offset: String, limit: String) {
        getProductCalled = true
    }
}
