//
//  sProductos.swift
//  tabBarIbk
//
//  Created by Derly Edwin Ticse Zuñiga on 12/06/24.
//

import Foundation


public class sProducto : Codable {
   public var id : Int?
   public var title : String?
   public var price : Int?
   public var description : String?
   public var category : sCategory?
   public var images : [String]?
    
    public init(id: Int?,title: String?,price: Int?,description: String?,category: sCategory, images:[String]?){
        self.id = id
        self.title = title
        self.price = price
        self.description = description
        self.category = category
        self.images = images
    }
}
public class sCategory : Codable {
    public var id: Int?
    public var name: String?
    public var image: String?
    
    public init(id: Int?,name: String?, image: String?){
        self.id = id
        self.name = name
        self.image = image
    }
}
/*
public struct sProducto : Codable {
    var id : Int?;
    var title : String?;
    var price : Int?;
    var description : String?;
    var category : sCategory?;
    var images : [String]?;
}
public struct sCategory : Codable {
    var id: Int?;
    var name: String?;
    var image: String?;
}
*/
