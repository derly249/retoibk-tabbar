//
//  sCategoria.swift
//  tabBarIbk
//
//  Created by Derly Edwin Ticse Zuñiga on 15/06/24.
//

import Foundation

public class sCategoria : Codable {
    public var id: Int?
    public var name: String?
    public var image: String?
    
    public init(id: Int?,name: String?, image: String?){
        self.id = id
        self.name = name
        self.image = image
    }
}
