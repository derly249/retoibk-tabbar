//
//  ShareUserDefaults.swift
//  webcontrolOP
//
//  Created by WCS on 14/09/18.
//  Copyright © 2018 WCS. All rights reserved.
//

import Foundation
import UIKit
import Network
import LocalAuthentication
//import Kingfisher

public class ShareUserDefaults{
    //private static UserDefaults preferences
    let defaults = UserDefaults.standard
    
    public let  PREF_USER = "pref_user";
    public let  PREF_PASS = "pref_pass";
    public let  PREF_RECORDARUSER = "pref_recordaruser";
    public let  PREF_USAHUELLA = "pref_usahuella";
    
    public let PREF_TOKEN = "pref_token" // Token para Anglo

    public let PREF_BACKGROUND_TABBAR = "pref_background_tabbar"

    public func getPrefToken() -> String {
        let token = defaults.object(forKey: PREF_TOKEN) as? String ?? ""
        return token
    }
    
    public func setPrefToken(token: String) {
        defaults.set(token, forKey: PREF_TOKEN)
    }
    
    public func getBackGroundTabBar() -> String {
        let Color = defaults.object(forKey: PREF_BACKGROUND_TABBAR) as? String ?? ""
        return Color
    }
    
    public func setBackGroundTabBar(pref: String?) {
        defaults.set(pref, forKey: PREF_BACKGROUND_TABBAR)
    }
    
    
    public func showSimpleAlert(titulo: String, mensaje: String, vc: UIViewController, okBtn: String = "Aceptar"){
        let alertOffline = UIAlertController(title: titulo, message: mensaje, preferredStyle: .alert)
        alertOffline.addAction(UIAlertAction(title: okBtn, style: .default, handler: nil))
        vc.present(alertOffline, animated: true, completion: nil)
    }
  
}
