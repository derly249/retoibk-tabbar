//
//  ProgressView.swift
//  Customer
//
//  Created by HAROL GOMEZ RUIZ on 27/5/18.
//  Copyright © 2018 HAROL GOMEZ RUIZ. All rights reserved.
//

import Foundation
import MBProgressHUD

class ProgressView {
    
    static var backgroundView = UIView()
    
    class func showSpinner(inView: UIView){
        let spinner = MBProgressHUD.showAdded(to: inView, animated: true)
        spinner.label.text = "LoadingText".localized;
    }
    
    class func hideSpinner(inView: UIView){
        MBProgressHUD.hide(for: inView, animated: true)
    }
    
    class func showSpinner(inView: UIView, withTitle title:String){
        let spinner = MBProgressHUD.showAdded(to: inView, animated: true)
        spinner.label.text = title
    }
    
    class func showSpinnerFullScreen(in view: UIView){
        guard let window = UIApplication.shared.keyWindow else { return }
        backgroundView.frame = window.bounds
        window.addSubview(backgroundView)
        backgroundView.backgroundColor = .clear
        showSpinner(inView: backgroundView)
    }
    
    class func hideSpinnerFullScreen(in view: UIView) {
        hideSpinner(inView: view)
        backgroundView.removeFromSuperview()
    }
}
