//
//  urlBuilder.swift
//  tabBarIbk
//
//  Created by Derly Edwin Ticse Zuñiga on 14/06/24.
//

import Foundation

enum urlPath : String {
    /** Api Platzi **/
    case BASE_URL = "http://api.escuelajs.co/api/v1/"

    case PRODUCTOS_URL = "http://api.escuelajs.co/api/v1/products"
    case CATEGORIAS_URL = "http://api.escuelajs.co/api/v1/categories"
}
