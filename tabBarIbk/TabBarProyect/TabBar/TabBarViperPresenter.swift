
import UIKit

protocol TabBarViperPresenterProtocol: AnyObject {
    
}

class TabBarViperPresenter {
    
    private unowned let view: TabBarViperViewControllerProtocol
    private let interactor: TabBarViperInteractorProtocol
    private let router: TabBarViperRouterProtocol
    private var viewData: TabBarViperViewData
    
    init(view: TabBarViperViewControllerProtocol,
         interactor: TabBarViperInteractorProtocol,
         router: TabBarViperRouterProtocol,
         viewData: TabBarViperViewData) {
        self.view = view
        self.interactor = interactor
        self.router = router
        self.viewData = viewData
    }
}

extension TabBarViperPresenter: TabBarViperPresenterProtocol {
    
}
