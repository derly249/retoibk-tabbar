
import UIKit

protocol TabBarViperRouterProtocol: AnyObject {
}

class TabBarViperRouter {
    
    var navigation: UINavigationController
    
    init(navigation: UINavigationController) {
        self.navigation = navigation
    }
    
    func instantiateViewController(viewData: TabBarViperViewData) -> UIViewController {
        
        let vc = TabBarViperViewController(nibName: "TabBarViperViewController", bundle: nil)
        let presenter = TabBarViperPresenter(view: vc,
        interactor: TabBarViperInteractor(),
        router: self,
        viewData: viewData)
        vc.setPresenter(presenter: presenter)
        return vc
    }
}

extension TabBarViperRouter: TabBarViperRouterProtocol {
}
