
import UIKit

protocol TabBarViperViewControllerProtocol: AnyObject{
    
}

class TabBarViperViewController: UITabBarController{

    private var presenter: TabBarViperPresenterProtocol?

    override func viewDidLoad(){
        super.viewDidLoad()
        let colorGuardado = ShareUserDefaults().getBackGroundTabBar()
        self.tabBar.backgroundColor = UIColor(hex: colorGuardado)

        setupTabs()

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(canbiarColorTabBar(_:)),
                                               name: NSNotification.Name("canbiarColorTabBar"),
                                               object: nil)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self);
    }
    
    @IBOutlet weak var primerTab: UITabBarItem!
    
    func setupTabs(){
        let products = ProductsRouter().instantiateViewController()
        let categorias = CategoriasRouter().instantiateViewController()
        let menus = MenusViewController()
        
        products.title = "Productos"
        categorias.title = "Categorias"
        menus.title = "Menu"
        self.setViewControllers([products,categorias,menus], animated: false)
        
        guard let items = self.tabBar.items else { return }
        
        let images = ["house", "star", "bell"]
        for x in 0...2{
            items[x].image = UIImage(systemName: images[x])
        }
    }
    
    
    func setPresenter(presenter: TabBarViperPresenterProtocol) {
        self.presenter = presenter
    }
    
    @objc func canbiarColorTabBar(_ notification: Notification){
        if let data = notification.userInfo as? [String:String] {
            self.tabBar.backgroundColor = UIColor(hex: data.first!.value)
        }
    }
}

extension TabBarViperViewController: TabBarViperViewControllerProtocol{
    
}

