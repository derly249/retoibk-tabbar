
import UIKit

protocol CategoriasPresenterProtocol: AnyObject {
    func getCategoria()
}

class CategoriasPresenter {
    
    private unowned let view: CategoriasViewControllerProtocol
    private let interactor: CategoriasInteractorProtocol
    private let router: CategoriasRouterProtocol
    
    init(view: CategoriasViewControllerProtocol,
         router: CategoriasRouterProtocol) {
        self.view = view
        self.interactor = CategoriasInteractor(repository: ProductRepository.sharedInstance)
        self.router = router
    }
}

extension CategoriasPresenter: CategoriasPresenterProtocol {
   func getCategoria(){
        self.view.startLoading()
        self.interactor.getCategorias()
            .subscribe(onNext: {[weak self] response in
                guard let `self` = self else { return }
                self.view.finishedLoading()
                self.view.setdataCategoria(datos: response)
            }, onError: {error in
                print("este error get", error)
            })
    }
}

