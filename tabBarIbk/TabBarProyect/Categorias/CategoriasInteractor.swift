
import UIKit
import RxSwift

protocol CategoriasInteractorProtocol: AnyObject {
    func getCategorias()-> Observable<[sCategoria]>
}

class CategoriasInteractor:Interactor,CategoriasInteractorProtocol {
    public  func getCategorias()-> Observable<[sCategoria]> {
        return (self.repository as! ProductRepository).getCategorias()
    }
}

