
import UIKit
import JGProgressHUD
import SDWebImage

protocol CategoriasViewControllerProtocol: AnyObject{
    func startLoading()
    func finishedLoading() 
    func setdataCategoria(datos: [sCategoria])
}

class CategoriasViewController: UIViewController,CategoriasViewControllerProtocol{
    @IBOutlet weak var categoriaTB: UITableView!
    
    private var presenter: CategoriasPresenterProtocol?
    
    let loader = JGProgressHUD(style: .light)
    let categoriaCellId = "CategoriaTableViewCell"
    var dataCategoria:[sCategoria] = []
    var isLoading = false

    override func viewDidLoad(){
        super.viewDidLoad()
        initTableView()
        cargarCategoria()
    }
    
    func setPresenter(presenter: CategoriasPresenterProtocol) {
        self.presenter = presenter
    }
    func startLoading() {
        loader.show(in: self.view)
    }
    
    func finishedLoading() {
        self.loader.dismiss()
    }
    
    func initTableView(){
        categoriaTB.delegate = self
        categoriaTB.dataSource = self
        categoriaTB.register(UINib.init(nibName: categoriaCellId , bundle: nil), forCellReuseIdentifier: categoriaCellId)
        categoriaTB.rowHeight = 80
    }

    func cargarCategoria(){
        self.presenter?.getCategoria()
    }
   
    func setdataCategoria(datos: [sCategoria]){
        if (datos.count != 0){
            dataCategoria.append(contentsOf: datos)
            categoriaTB.reloadData()
            isLoading = false
        }
    }
}

extension CategoriasViewController:UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataCategoria.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = categoriaTB.dequeueReusableCell(withIdentifier: categoriaCellId,for: indexPath) as? CategoriaTableViewCell else {
            return UITableViewCell()
        }
        let categoria = dataCategoria[indexPath.row]
        cell.lblName.text = categoria.name
        cell.lblId.text = "\(categoria.id ?? 0)"
        
        if let imageUrlString = categoria.image, let imageUrl = URL(string: imageUrlString), !imageUrlString.isEmpty {
            cell.imgCategoria.sd_setImage(with: imageUrl, placeholderImage: UIImage(systemName: "photo"))
        } else {
            cell.imgCategoria.image = UIImage(systemName: "photo")
            print("No hay imágenes disponibles")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastItem = dataCategoria.count - 1
        // Verificar si es la última celda y si no se está cargando
        print("indextPath",indexPath.row)
        print("lasitem",lastItem)
        if indexPath.row == lastItem && !isLoading{
            let offset = dataCategoria.count + 5
            let limit = 5
            cargarCategoria()
        }
    }
}
