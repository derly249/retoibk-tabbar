
import UIKit

protocol CategoriasRouterProtocol: AnyObject {
}

class CategoriasRouter {
    

    func instantiateViewController() -> UIViewController {
        
        let vc = CategoriasViewController(nibName: "CategoriasViewController", bundle: nil)
        let presenter = CategoriasPresenter(view: vc as! CategoriasViewControllerProtocol,
        router: self)
        vc.setPresenter(presenter: presenter)
        return vc
    }
}

extension CategoriasRouter: CategoriasRouterProtocol {
}
