//
//  CategoriaTableViewCell.swift
//  tabBarIbk
//
//  Created by Derly Edwin Ticse Zuñiga on 15/06/24.
//

import UIKit

class CategoriaTableViewCell: UITableViewCell {

    @IBOutlet weak var imgCategoria: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblId: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
