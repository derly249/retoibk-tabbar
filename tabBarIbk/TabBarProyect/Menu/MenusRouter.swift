
import UIKit

protocol MenusRouterProtocol: AnyObject {
}

class MenusRouter {
    
    var navigation: UINavigationController
    
    init(navigation: UINavigationController) {
        self.navigation = navigation
    }
    
    func instantiateViewController(viewData: MenusViewData) -> UIViewController {
        
        let vc = MenusViewController(nibName: "MenusViewController", bundle: nil)
        let presenter = MenusPresenter(view: vc,
        interactor: MenusInteractor(),
        router: self,
        viewData: viewData)
        vc.setPresenter(presenter: presenter)
        return vc
    }
}

extension MenusRouter: MenusRouterProtocol {
}
