
import UIKit

protocol menuDelegate{
    func menuColorComplete(color: String)
}
protocol MenusViewControllerProtocol: AnyObject{
    
}

class MenusViewController: UIViewController{
    
    @IBOutlet weak var colorWell: UIColorWell!
    private var presenter: MenusPresenterProtocol?
    
    var delegate: menuDelegate?
    
    override func viewDidLoad(){
        super.viewDidLoad()

        let colorGuardado = ShareUserDefaults().getBackGroundTabBar()
        colorWell.selectedColor = UIColor(hex: colorGuardado)
        colorWell.addTarget(self, action: #selector(colorWellChanged(_:)), for: .valueChanged)
    }
    
    @objc func colorWellChanged(_ sender: Any){
        delegate?.menuColorComplete(color: colorWell.selectedColor!.toHexString()!)
        ShareUserDefaults().setBackGroundTabBar(pref: colorWell.selectedColor?.toHexString())

        let info = ["color" : colorWell.selectedColor?.toHexString()]
        NotificationCenter.default.post(name: Notification.Name(rawValue: "canbiarColorTabBar"), object: self, userInfo: info as [AnyHashable : Any])
    }
    
    func setPresenter(presenter: MenusPresenterProtocol) {
        self.presenter = presenter
    }
}

extension MenusViewController: MenusViewControllerProtocol{
    
}
