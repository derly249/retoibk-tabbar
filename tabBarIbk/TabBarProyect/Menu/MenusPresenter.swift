
import UIKit

protocol MenusPresenterProtocol: AnyObject {
    
}

class MenusPresenter {
    
    private unowned let view: MenusViewControllerProtocol
    private let interactor: MenusInteractorProtocol
    private let router: MenusRouterProtocol
    private var viewData: MenusViewData
    
    init(view: MenusViewControllerProtocol,
         interactor: MenusInteractorProtocol,
         router: MenusRouterProtocol,
         viewData: MenusViewData) {
        self.view = view
        self.interactor = interactor
        self.router = router
        self.viewData = viewData
    }
}

extension MenusPresenter: MenusPresenterProtocol {
    
}
