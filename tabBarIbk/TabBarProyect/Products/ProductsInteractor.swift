
import UIKit
import RxSwift

public protocol ProductsInteractorProtocol: AnyObject {
    func getProduct(offset: String,limit: String)-> Observable<[sProducto]>

}

class ProductsInteractor:Interactor,ProductsInteractorProtocol {
    public  func getProduct(offset: String,
                            limit: String)-> Observable<[sProducto]> {
        return (self.repository as! ProductRepository).getProduct(offset: offset, limit: limit)
    }
}



