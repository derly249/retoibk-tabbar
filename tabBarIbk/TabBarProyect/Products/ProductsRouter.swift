
import UIKit

protocol ProductsRouterProtocol: AnyObject {
}

class ProductsRouter {
    
    func instantiateViewController() -> UIViewController {
        
        let vc = ProductsViewController(nibName: "ProductsViewController", bundle: nil)
        let presenter = ProductsPresenter(view: vc as! ProductsViewControllerProtocol,
        router: self)
        vc.setPresenter(presenter: presenter)
        return vc
        
        
    }
}

extension ProductsRouter: ProductsRouterProtocol {
}
