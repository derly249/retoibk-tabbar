
import UIKit
import JGProgressHUD
import SDWebImage

protocol ProductsViewControllerProtocol: AnyObject{
    func startLoading()
    func finishedLoading() 
    func setdataProducto(datos: [sProducto])
}

class ProductsViewController: UIViewController, ProductsViewControllerProtocol{

    @IBOutlet weak var productTB: UITableView!
    
    private var presenter: ProductsPresenterProtocol!

    let loader = JGProgressHUD(style: .light)
    let productCellId = "ProductTableViewCell"
    var dataProducto:[sProducto] = []
    var isLoading = false
    

    override func viewDidLoad(){
        super.viewDidLoad()
        initTableView()
        cargarProducto()
    }

    func setPresenter(presenter: ProductsPresenterProtocol) {
        self.presenter = presenter
    }
    
    func initTableView(){
        productTB.delegate = self
        productTB.dataSource = self
        productTB.register(UINib.init(nibName: productCellId , bundle: nil), forCellReuseIdentifier: productCellId)
        productTB.rowHeight = 80
    }
    
    func startLoading() {
        loader.show(in: self.view)
    }

    func finishedLoading() {
        self.loader.dismiss()
    }
    func cargarProducto(offset: String = "0", limit: String = "5"){
        self.presenter.getproducto(offset: offset, limit: limit)
    }
   
    func setdataProducto(datos: [sProducto]){
        if (datos.count != 0){
            dataProducto.append(contentsOf: datos)
            productTB.reloadData()
            isLoading = false
        }
        
    }
}


extension ProductsViewController:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataProducto.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = productTB.dequeueReusableCell(withIdentifier: productCellId,for: indexPath) as? ProductTableViewCell else {
            return UITableViewCell()
        }
        let product = dataProducto[indexPath.row]
        if let price = product.price {
            cell.lblCategoria.text = "Precio: \(price) Soles"
        } else {
            cell.lblCategoria.text = "Precio no disponible"
        }
        
        if let imageUrlString = product.images?.first, let imageUrl = URL(string: imageUrlString), !imageUrlString.isEmpty {
            cell.imgProduct.sd_setImage(with: imageUrl, placeholderImage: UIImage(systemName: "photo"))
        } else {
            cell.imgProduct.image = UIImage(systemName: "photo")
            print("No hay imágenes disponibles")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastItem = dataProducto.count - 1
        // Verificar si es la última celda y si no se está cargando
        print("indextPath",indexPath.row)
        print("lasitem",lastItem)
        if indexPath.row == lastItem && !isLoading{
            let offset = dataProducto.count + 5
            let limit = 5
            cargarProducto(offset: "\(offset)", limit: "\(limit)")
        }
    }
}

