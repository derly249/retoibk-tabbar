
import UIKit

protocol ProductsPresenterProtocol: AnyObject {
    func getproducto(offset: String,limit: String)
}

class ProductsPresenter {

    private unowned let view: ProductsViewControllerProtocol
    private let interactor: ProductsInteractorProtocol
    private let router: ProductsRouterProtocol
    
    init(view: ProductsViewControllerProtocol,
         router: ProductsRouterProtocol) {
        self.view = view
        self.interactor = ProductsInteractor(repository: ProductRepository.sharedInstance)
        self.router = router
    }
}

extension ProductsPresenter: ProductsPresenterProtocol {
    func getproducto(offset: String,limit: String){
        self.view.startLoading()
         self.interactor.getProduct(offset: offset, limit: limit)
            .subscribe(onNext: {[weak self] response in
                guard let `self` = self else { return }
                self.view.finishedLoading()
                self.view.setdataProducto(datos: response)
            }, onError: {error in
                print("este error get", error)
            })
    }
}
