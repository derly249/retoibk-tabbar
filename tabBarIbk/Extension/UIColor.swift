//
//  UIColor.swift
//  tabBarIbk
//
//  Created by Derly Edwin Ticse Zuñiga on 13/06/24.
//

import Foundation
import UIKit

extension UIColor {
    func toHexString() -> String? {
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0

        guard self.getRed(&r, green: &g, blue: &b, alpha: &a) else { return nil }

        let rgb: Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0

        return String(format: "#%06x", rgb)
    }
}



extension UIColor {
    convenience init?(hex: String) {
        var cleanedHexString = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        // Remove # if it appears
        if cleanedHexString.hasPrefix("#") {
            cleanedHexString.remove(at: cleanedHexString.startIndex)
        }

        // The string should be 6 or 8 characters
        guard cleanedHexString.count == 6 || cleanedHexString.count == 8 else {
            return nil
        }

        // If it's 6 characters long, add FF for alpha
        if cleanedHexString.count == 6 {
            cleanedHexString.append("FF")
        }

        // Convert hex string to an integer
        var rgbValue: UInt64 = 0
        Scanner(string: cleanedHexString).scanHexInt64(&rgbValue)

        // Extract and convert components
        let r = CGFloat((rgbValue & 0xFF000000) >> 24) / 255.0
        let g = CGFloat((rgbValue & 0x00FF0000) >> 16) / 255.0
        let b = CGFloat((rgbValue & 0x0000FF00) >> 8) / 255.0
        let a = CGFloat(rgbValue & 0x000000FF) / 255.0

        self.init(red: r, green: g, blue: b, alpha: a)
    }
}
