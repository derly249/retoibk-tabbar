//
//  ProductRepository.swift
//  tabBarIbk
//
//  Created by Derly Edwin Ticse Zuñiga on 14/06/24.
//

import Foundation
import RxSwift

protocol ProductRepositoryProtocol {
    func getProduct(offset: String,limit: String)-> Observable<[sProducto]>
    func getCategorias()-> Observable<[sCategoria]>
}

public class ProductRepository: ProductRepositoryProtocol {
    let restApiImpl: APIconnect = APIconnect.sharedInstance
    public static let sharedInstance = ProductRepository()
    init() {}
    
    func getCategorias()-> Observable<[sCategoria]> {
        let url = urlPath.CATEGORIAS_URL
          
        let response: Observable<[categoriaEntity]> = restApiImpl.callRestApiv4(url: url.rawValue, method: .get, body: nil, headers: nil)
        return response.map { categoriaEntity.mapperFromArray(arraycategoriaEntity: $0)! }
    }
    
    func getProduct(offset: String,limit: String)-> Observable<[sProducto]> {
        let url = urlPath.PRODUCTOS_URL
            let parameters: [String: String] = [
                "offset": offset,
                "limit": limit
            ]
        let urlWithParameters = addQueryParameters(to: url.rawValue, parameters: parameters)
            print("URL::", url)
            let response: Observable<[productoEntity]> = restApiImpl.callRestApiv4(url: urlWithParameters, method: .get, body: nil, headers: nil)
        return response.map { productoEntity.mapperFromArray(arrayproductoEntity: $0)! }
    }
    
    private func addQueryParameters(to url: String, parameters: [String: String]) -> String {
        var urlComponents = URLComponents(string: url)!
        urlComponents.queryItems = parameters.map { URLQueryItem(name: $0.key, value: $0.value) }
        return urlComponents.url!.absoluteString
    }
    
}


