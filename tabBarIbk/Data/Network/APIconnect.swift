//
//  File.swift
//  webcontrolOP
//
//  Created by WCS on 14/09/18.
//  Copyright © 2018 WCS. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift

public class APIconnect{
  
    public static let sharedInstance = APIconnect()
}


extension APIconnect {
    public func callRestApiv4<T: Decodable>(url: String, method: HTTPMethod, body: [String: Any]?, headers: [String: String]?) -> Observable<T> {
        return Observable.create { observer in
            let request = Alamofire.request(url, method: method, parameters: body, encoding: JSONEncoding.default, headers: headers)
                .validate()
                .responseData { response in
                    switch response.result {
                    case .success(let data):
                        do {
                            
                            let decoder = JSONDecoder()
                            let decodedObject = try decoder.decode(T.self, from: data)
                            observer.onNext(decodedObject)
                            observer.onCompleted()
                        } catch let error {
                            observer.onError(error)
                        }
                    case .failure(let error):
                        observer.onError(error)
                    }
                }
                return Disposables.create {
                request.cancel()
            }
        }
    }
}
    
  
