//
//  categoriaEntity.swift
//  tabBarIbk
//
//  Created by Derly Edwin Ticse Zuñiga on 15/06/24.
//

import Foundation

class categoriaEntity: Decodable{
    public var id: Int?
    public var name: String?
    public var image: String?
    private enum CodingKeys : String, CodingKey {
        case id
        case name
        case image
    }
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try? container.decode(Int.self, forKey: .id)
        self.name = try? container.decode(String.self, forKey: .name)
        self.image = try? container.decode(String.self, forKey: .image)
    }
}
extension categoriaEntity{
    class func mapper(entity: categoriaEntity) -> sCategoria {
        return sCategoria(id: entity.id, name: entity.name, image: entity.image)
    }
    class func mapperFromArray(arraycategoriaEntity:[categoriaEntity]?)-> [sCategoria]?{
        if let arraycategoria = arraycategoriaEntity{
            var arrayCategory:[sCategoria] = []
            
            for categoryEntity in arraycategoria {
                arrayCategory.append(mapper(entity: categoryEntity))
            }
            return arrayCategory
        }else {
            return nil
        }
      }
}
