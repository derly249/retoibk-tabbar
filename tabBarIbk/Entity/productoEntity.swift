//
//  productoEntity.swift
//  tabBarIbk
//
//  Created by Derly Edwin Ticse Zuñiga on 14/06/24.
//

import Foundation

class categorysEntity: Decodable{
    public var id: Int?
    public var name: String?
    public var image: String?
    private enum CodingKeys : String, CodingKey {
        case id
        case name
        case image
    }
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try? container.decode(Int.self, forKey: .id)
        self.name = try? container.decode(String.self, forKey: .name)
        self.image = try? container.decode(String.self, forKey: .image)
    }
}
extension categorysEntity{
    class func mapper(entity: categorysEntity) -> sCategory {
        return sCategory(id: entity.id, name: entity.name, image: entity.image)
    }
    class func mapperFromArray(arrayCategorysEntity:[categorysEntity]?)-> [sCategory]?{
        if let arrayCategorys = arrayCategorysEntity{
            var arrayCategory:[sCategory] = []
            
            for categoryEntity in arrayCategorys {
                arrayCategory.append(mapper(entity: categoryEntity))
            }
            return arrayCategory
        }else {
            return nil
        }
      }
}
class productoEntity: Decodable{
    public var id: Int?
    public var title: String?
    public var price: Int?
    public var description: String?
    public var category: categorysEntity?
    public var images: [String]?
    
    private enum CodingKeys : String, CodingKey {
        case id = "id"
        case title
        case price
        case description
        case category
        case images
    }
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try? container.decode(Int.self, forKey: .id)
        self.title = try? container.decode(String.self, forKey: .title)
        self.price = try? container.decode(Int.self, forKey: .price)
        self.description = try? container.decode(String.self, forKey: .description)
        self.category = try? container.decode(categorysEntity.self, forKey: .category)
        self.images = try? container.decode([String].self, forKey: .category)

    }
}

extension productoEntity{
    class func mapper(entity: productoEntity ) -> sProducto {
        return sProducto(id: entity.id,
                         title: entity.title,
                         price: entity.price,
                         description: entity.description,
                         category: categorysEntity.mapper(entity: entity.category!),
                         images: entity.images)
    }
    
    class func mapperFromArray(arrayproductoEntity:[productoEntity]?)-> [sProducto]?{
        if let arrayProducts = arrayproductoEntity{
            var arrayProducto:[sProducto] = []
            for productoEntity in arrayProducts {
                arrayProducto.append(mapper(entity:productoEntity ))
            }
            return arrayProducto
        }else {
            return nil
        }
      }
    
}
